package com.dev.productservice;

import com.dev.productservice.model.Product;
import com.dev.productservice.repo.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;

@SpringBootApplication
public class ProductServiceApplication implements CommandLineRunner {

    private final ProductRepo productRepo;
    private final RepositoryRestConfiguration repositoryRestConfiguration;

    @Autowired
    public ProductServiceApplication(ProductRepo productRepo, RepositoryRestConfiguration repositoryRestConfiguration) {
        this.productRepo = productRepo;
        this.repositoryRestConfiguration = repositoryRestConfiguration;
    }

    public static void main(String[] args) {
        SpringApplication.run(ProductServiceApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        repositoryRestConfiguration.exposeIdsFor(Product.class);

        productRepo.save(new Product(null, "Master and Margaret", 5, 50));
        productRepo.save(new Product(null, "The Little Prince", 3, 50));
        productRepo.save(new Product(null, "Lalala", 5, 50));
        productRepo.save(new Product(null, "Lululu", 5, 50));
        productRepo.save(new Product(null, "BOOM", 3, 50));
        productRepo.save(new Product(null, "A Little Girl", 3, 50));

        productRepo.findAll().forEach(p -> {
            System.out.println(p.toString());
        });
    }

}
