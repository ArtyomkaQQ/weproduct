package com.dev.productservice.controller;

import com.dev.productservice.model.Product;
import com.dev.productservice.repo.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductRestServices {

    private final ProductRepo productRepo;

    @Autowired
    public ProductRestServices(ProductRepo productRepo) {
        this.productRepo = productRepo;
    }

    @GetMapping(value = "/listproducts")
    public List<Product> listProducts() {
        return productRepo.findAll();
    }

    @GetMapping(value = "/listproducts/{id}")
    public Product listProductsById(@PathVariable(name = "id") Long id) {
        return productRepo.findById(id).get();
    }

    @PutMapping(value = "/listproducts/{id}")
    public Product update(@PathVariable(name = "id") Long id, @RequestBody Product product) {
        product.setId(id);

        return productRepo.save(product);
    }

    @PostMapping(value = "/listproducts")
    public Product save(@RequestBody Product product) {
        return productRepo.save(product);
    }

    @PostMapping(value = "/listproducts/{id}")
    public void delete(@PathVariable(name = "id") Long id) {
        productRepo.deleteById(id);
    }

}
