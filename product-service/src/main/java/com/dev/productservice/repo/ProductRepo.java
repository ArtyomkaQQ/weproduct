package com.dev.productservice.repo;

import com.dev.productservice.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin("*")
@RepositoryRestResource
public interface ProductRepo extends JpaRepository<Product, Long> {

    @RestResource(path = "/byDescription")
    public List<Product> findByDescriptionContains(@Param("mc") String desc);

    @RestResource(path = "/byDescriptionPage")
    public Page<Product> findByDescriptionContains(@Param("mc") String desc, Pageable pageable);

}
