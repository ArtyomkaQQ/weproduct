import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CatalogService} from '../services/catalog.service';
import {Product} from '../model/product.model';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
  public currentProduct: Product;
  public url: string;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private catalogService: CatalogService) { }

  ngOnInit() {
    this.url = atob(this.activatedRoute.snapshot.params.id);
    this.catalogService.getProduct(this.url)
      .subscribe(data => {
        this.currentProduct = data;
      }, err => {
        console.log(err);
      })
  }

  onUpdateProduct(value: any) {
    this.catalogService.updateProduct(this.url, value)
      .subscribe(data => {
        alert("Update succeeded")
        this.router.navigateByUrl("/products");
      }, err => {
        console.log(err);
      })
  }
}
