import {Component, OnInit} from '@angular/core';
import {CatalogService} from '../services/catalog.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  public products:any;
  public size: number = 5;
  public currentPage: number = 0;
  public totalPages: number;
  public pages: Array<number>;
  public currentKeyword: string="";

  constructor(private catalogService: CatalogService, private router: Router) { }

  ngOnInit() {
  }

  onGetProducts() {
    this.catalogService.getProducts(this.currentPage, this.size)
      .subscribe(data => {
        this.totalPages = data["page"].totalPages;
        this.pages = new Array<number>(this.totalPages);
        this.products = data;
      }, err => {
        console.log(err);
      })
  }

  onPageProduct(i: number) {
    this.currentPage = i;
    this.searchProduct();
  }

  onSearch(form: any) {
    this.currentPage = 0;
    this.currentKeyword = form.keyword;
    this.searchProduct();
  }

  searchProduct() {
    this.catalogService.getProductsByKeyword(this.currentKeyword, this.currentPage, this.size)
      .subscribe(data => {
        this.totalPages = data["page"].totalPages;
        this.pages = new Array<number>(this.totalPages);
        this.products = data;
      }, err => {
        console.log(err);
      })
  }

  onDeleteProduct(p) {
    let conf = confirm("Are You sure?");
    if (conf) {
      this.catalogService.deleteProduct(p._links.self.href)
        .subscribe(data => {
          this.searchProduct();
        }, err => {
          console.log(err);
        })
    }
  }

  onEditProduct(p) {
    let url = p._links.self.href;
    this.router.navigateByUrl("/edit-product/" + btoa(url));
  }

}
