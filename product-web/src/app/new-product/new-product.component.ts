import { Component, OnInit } from '@angular/core';
import {CatalogService} from '../services/catalog.service';
import {Router} from '@angular/router';
import {Product} from '../model/product.model';

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.css']
})
export class NewProductComponent implements OnInit {
  public currentProduct: Product;
  public mode: number = 1;
  // mode 1 - add new product
  // mode 2 - show added new product

  constructor(private catalogService: CatalogService, private router: Router) { }

  ngOnInit() {
  }

  onSaveProduct(data: any) {
    this.catalogService.saveProduct(this.catalogService.host + "/products", data)
      .subscribe(res => {
        // this.router.navigateByUrl("/products")
        this.currentProduct = res;
        this.mode = 2;
      }, err => {
        console.log(err);
      })
  }

  onNewProduct() {
    this.mode = 1;
  }

}
